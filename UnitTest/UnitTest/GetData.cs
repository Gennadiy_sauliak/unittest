﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace UnitTest
{
    class User
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }

    class DataUser
    {
        public List<User> DataUsers { get; set; }
    }

    class GetData
    {
        private DataUser data;

        public GetData(string pathJsonFile)
        {
            string Json;
            using (StreamReader file = File.OpenText(pathJsonFile))
            {
                JsonSerializer serializer = new JsonSerializer();
                Json = file.ReadToEnd();
            }

            data = JsonConvert.DeserializeObject(Json, typeof(DataUser)) as DataUser;

        }
        public User this[int index]
        {
            get
            {
                return data.DataUsers[index];
            }
        }
   
    }
}
