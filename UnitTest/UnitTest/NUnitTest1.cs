﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;



namespace UnitTest
{
    [TestFixture]
    public class NUnitTest1
    {
        private IWebDriver browser = new ChromeDriver();

        GetData d;

        string pathJsonFile = @"g:\UnitTest\UnitTest\UnitTest\TestData.json";

        string startUrl = "https://vk.com/";

        [SetUp]
        public void Init()
        {
            var location = System.Reflection.Assembly.GetExecutingAssembly().Location;

            d = new GetData(pathJsonFile);

            browser.Navigate().GoToUrl(startUrl);
           
        

        }

        [Test]
        public void AuthTest()
        {
            IWebElement l = browser.FindElement(By.Id("index_email"));
            l.Click();
            l.Clear();
            l.SendKeys(d[1].Login);
           
            IWebElement p = browser.FindElement(By.Id("index_pass"));
            p.Click();
            p.Clear();
            p.SendKeys(d[1].Password);

            browser.FindElement(By.Id("index_login_button")).Click();

            Thread.Sleep(200);
        }
       


        [TearDown]
        public void EndTest()
        {
           // browser.Close();
        }

    }
}